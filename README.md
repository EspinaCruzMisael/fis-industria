```plantuml
@startmindmap
+[#Orange] Industria 4.0
++_ consiste en
+++[#Azure] la generacion de nuevas maquinas con produccion\nautonoma para que exista cada vez menos\n intervencion humana
++++_ cambios que genera 
+++++[#Gold] Integracion de TICs en la industria\n de manufactura y de servicios
+++++[#Coral] Aparicion de nuevas profesiones el \n el futuro
+++++[#Red] Transformacion de las empresas de manufactura\n en empresas de TICs
+++++[#Green] Aparicion de Nuevas Profeciones 
++++++_ como 
+++++++[#Beige] Youtubers 
+++++++[#Beige] Desarrolladores de Apps 
+++++++[#Beige] Pilotos de Drones 
+++++++[#Beige] Consultores de redes Sociales
+++++[#Crimson] Nuevos paradigmas y tecnologias
++++++_ tales como  
+++++++[#Purple] velocidad: que es la capacidad que deben de tener las\nempresas para adaptarse a los cambios tecnologicos
+++++++[#Purple] negocios basados en plataforma: Son empresas que basan \n sus negocios en plataformas tecnologicas.
+++++++[#Purple] Internet de las cosas: que es la capacidad que ya tienen la mayoria \nde los productos para que sean conectados via internet, ya sea via internet o \n sensores, radio frecuencias entre otras.
+++++++[#Purple] Big data e inteligencia artifical
+++++++[#Purple] Otras tecnologias 
++++++++_ como 
+++++++++[#Grey] carros autonomos
+++++++++[#Grey] realidad virtual 
+++++++++[#Grey] realidad aumentada 
+++++++++[#Grey] comunicaciones 5g
+++++[#White] Nuevas culturas Digitales
++++++_ entre ellos 
+++++++[#Pink] La mayoria de las personas usan la tecnologia \n como parte normal de su vida
+++++++[#Pink] Manejan muy bien las ultimas tecnologias.
+++++++[#Pink] Una mayor dependencia a la Tecnologia
+++++++[#Pink] Creacion de deportes electronicos
@endmindmap
```

```plantuml
@startmindmap
+[#White] Industria 4.0\n en México 
++_ Y 
+++[#Gold] Como entran las personas en la \ industria 4.0 en Mexico 
++++_ Entran como 
+++++[#Yellow] Las Personas que le dicen a la\n maquina que hacer
+++++[#Yellow] Las  Personas que obedecen a\n la maquina
+++[#Red] De que trata 
++++_ De
+++++[#Salmon] De transformar los empleos\n en lugar de quitarlos.
+++[#Pink] Donde esta México.
++++_ Esta en
+++++[#Beige] En el punto donde se tienen que \n empezar a crear mas perfiles laborales \n en base a las industrias en el pais.
+++[#Azure] Contras que se deriban en Mexico
++++_ Tales Como
+++++[#Brown] La seguridad que pueden tener las empresas 
+++++[#Brown] Las malas Infrestructuras que tiene Mexico 
+++++[#Brown] Tardio Acceso a las Nuevas Tecnologias 
+++++[#Brown] Desconocimineto de las tecnologias inteligentes\n ya que al no saber de estas hay muchas empresas\n  que no usan estas tecnologias y tambien se piensa que son muy caras\n y no todas lo son.
+++[#Orange] Que necesita mexico para seguir progresando
++++_ Necesita de
+++++[#Grey] Disponer de Buenas Infrestructuras
+++++[#Grey] Tener Acceso a internet y a las nuevas Tecnologias
+++[#Olive] Que busca las empresas ques \n se encuentran en Mexico en la\n industria 4.0
++++_ Buscan
+++++[#Lightgreen] Optimizar Puestos 
+++++[#Lightgreen] Hacer mas invirtiendo menos 
+++++[#Lightgreen] Creacion de Nuevas Areas de Negocio 
+++[#Darkorange] Camino De Desarrollo en la Industria 4.0
++++_ Como
+++++[#Lightblue] Visibilidad 
+++++[#Lightblue] Transparencia
+++++[#Lightblue] Previsibilidad 
+++++[#Lightblue] Adaptabilidad
+++[#Coral] Como se aplica en México 
++++_ Por ejemplo
+++++[#Azure] Se aplica por ejemplo en Algunos supermercados en México\n a la hora de cobrar ya que son cajas sin personas.
+++++[#Azure] O a la hora de definir las rutas para la entrega de \n productos mediante la inteligencia artifial.
+++++[#Azure] Otro ejemplo es en las plataformas maritimas de pemex\n, a la hora de hacer mantenimiento en tiempo real para saber\nsi algo va a fallar ya que se detecta horas o dias antes de que falle.
@endmindmap
```
